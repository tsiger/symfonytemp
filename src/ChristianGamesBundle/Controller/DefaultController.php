<?php

namespace ChristianGamesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /*$em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('ChristianGamesBundle:Game');
        
        $games = $repo->findAll();
        $source = new Entity('ChristianGamesBundle:Game');
        $grid = $this->get('grid');

        $grid->setSource($source);

        return $grid->getGridResponse('ChristianGamesBundle:Default:index.html.twig');*/
        
        
        
        $source = new Entity('ChristianGamesBundle:Game');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');

        $grid->setSource($source);

        $res = $grid->getGridResponse('ChristianGamesBundle:Default:index.html.twig');
        return $res;
        //return $this->render('ChristianGamesBundle:Default:index.html.twig', ['games' => $games]);
    }
    
    public function categoryAction($categoryName = '')
    {
        return $this->render('ChristianGamesBundle:Default:category.html.twig', ['category' => $categoryName]);
    }
}
